# Shadow Live OS

Shadow Live OS is a project based on Linux to create a Live OS (an OS loaded in the RAM) that can quickly run Shadow out of the box. The OS should be light and embed all libraries and drivers to be usable on all computers.

The Shadow Live OS uses only one base (Arch) for now but may implement others, the goal is to have the lightest ISO. Obviously, all kinds of help are welcome !

The Shadow Live OS works with both EFI and Legacy boot system. Moreover, this project was developped linked with the **[ShadowOS Networked boot]('https://gitlab.com/aar642/shadowos-boot')**. It allows to boot on a 1Mo iPXE file and download the latest stable ISO directly in the boot process.

You can find a variant based on Alpine Linux [here](https://gitlab.com/NicolasGuilloux/shadow-live-os/tree/alpine-master).


## Installation

Download the ISO of the latest tagged Shadow stable version from the pipeline or with this [link to Stable version](https://gitlab.com/NicolasGuilloux/shadow-live-os/-/jobs/225491088/artifacts/raw/ShadowLive-4.4.x.2-x86_64.iso) (769MB).

Download the ISO of the latest tagged Shadow beta version from the pipeline or with this [link to Beta version](https://gitlab.com/NicolasGuilloux/shadow-live-os/-/jobs/225491087/artifacts/raw/ShadowLive-4.4.x.2-Beta-x86_64.iso) (769MB).

The unstable version is available [here](https://gitlab.com/NicolasGuilloux/shadow-live-os/-/jobs/artifacts/arch-master/raw/ShadowLive-master-x86_64.iso?job=arch_build). ![Pipeline state](https://gitlab.com/NicolasGuilloux/shadow-live-os/badges/arch-master/build.svg?job=arch_build)

Flash the ISO using your favorite software, I recommand Etcher. Select first your ISO file, then your USB key and finally, flash.

When finished, plug the USB key on a computer while it is off, enter in the Boot menu (Esc when the logo is displayed for most Asus for instance) and select your USB device.

Do not remove the flash drive when you use the ISO directly from an USB key as it may get some file from the flash drive during usage. You can only remove the USB drive when the computer is completly shutdown.


## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)


## Contributing

To build the image, you need an Arch Linux computer with [archiso](https://wiki.archlinux.fr/archiso) installed.

Then, run the `build.sh` script as root to build the ISO file.


## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![GiantPandaRoux#0777](https://cdn.discordapp.com/avatars/267044035032514561/e98147b99f4821c4e806e97fda05e69a.png?size=64 "GiantPandaRoux#0777")
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")
![0x4cc3a96f#4425](https://secure.gravatar.com/avatar/41654d1de896d7560aefaa0e4169716d.png?size=64 "0x4cc3a96f#4425")

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
